# Retrieve raw events data

### Steps :

### Set up `yarn`

- set up the env, DB connects details are in .env file

### Create a postgres DB in local name `zixuan-postgres`

### Start the project `yarn dev`

- try to call API GET `localhost:3000`, should get return "Established connection!"

### Insert DB schema `yarn migration:run`

- check in DB, there should be 3 tables created
  - organizer : will use to save data from organizer CSV
  - smart contract: will use to save data from smart contracts JSON
  - migrations: have all ran migration files recorded

### Insert Data In DB

- call API POST `localhost:3000/insert-smart-contract-data`

  - this route read organizer CSV file and insert into DB organizer table

- call API POST `localhost:3000/insert-organizer-data`

  - this route read smart contract JSON file and insert into DB with relation if organizer

### Get Event Data API

- call API GET `localhost:3000/events` to get all events
- call API GET `localhost:3000/events?eventId=1` to get events filtered on eventId = 1
- call API GET `localhost:3000/events?saleStartAfter=2022-10-10` to get events filtered on saleStartAt is after 2022-10-10

### Update Event Data API

- call API PUT `localhost:3000/events/5` with body in formate JSON to update event = 5
  Body example:
  {
  "eventTitle": "test",
  "collectionName": "test update",
  "lineUp": ["test"],
  "media": "https://hellfest.com/test.png"
  }
  should return updated eventId = 5
