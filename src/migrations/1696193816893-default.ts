import { MigrationInterface, QueryRunner } from "typeorm";

export class Default1696193816893 implements MigrationInterface {
    name = 'Default1696193816893'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "smart-contract" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "organizerId" uuid, "eventId" integer NOT NULL, "collectionName" character varying NOT NULL, "crowdsale" character varying NOT NULL, "collection" character varying NOT NULL, "multisig" character varying NOT NULL, "isPresale" boolean NOT NULL, "metadataList" character varying NOT NULL, "pricePerToken" integer NOT NULL, "maxMintPerUser" integer NOT NULL, "saleSize" integer NOT NULL, "saleCurrency" json NOT NULL, CONSTRAINT "PK_625305ec6a34d23397212d41e8a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "organizer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "eventId" integer NOT NULL, "eventTitle" character varying NOT NULL, "eventStartDate" TIMESTAMP NOT NULL, "eventEndDate" TIMESTAMP NOT NULL, "addressName" character varying, "address" character varying NOT NULL, "totalTicketNumber" integer NOT NULL, "maxTicketPerUser" integer NOT NULL, "saleStartAt" TIMESTAMP NOT NULL, "lineUp" text array NOT NULL, "media" character varying, CONSTRAINT "PK_b59551a131f312443b992f90434" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "smart-contract" ADD CONSTRAINT "FK_3cecc207208092d4468ade60f08" FOREIGN KEY ("organizerId") REFERENCES "organizer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "smart-contract" DROP CONSTRAINT "FK_3cecc207208092d4468ade60f08"`);
        await queryRunner.query(`DROP TABLE "organizer"`);
        await queryRunner.query(`DROP TABLE "smart-contract"`);
    }

}
