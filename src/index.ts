import express from "express";
import { AppDataSource } from "./data-source";
import { getEvents, isError, updateEvent } from "./event/event";
import { createOrganizerActionFromCSV } from "./organizer/organizer";
import { createSmartContractAction } from "./smart-contract/smart-contract";
import moment from "moment";
import { globalDateFormat } from "./organizer/mapper";

const app = express();
app.use(express.json());

AppDataSource.initialize().then(() => {
  app.get("/", (req, res) => {
    return res.json("Established connection!");
  });

  app.post("/insert-organizer-data", async (req, res) => {
    const isSucceed = await createOrganizerActionFromCSV();
    if (isSucceed) {
      return res.status(200).json("OK! organizer data inserted!");
    }
    return res.status(500).json("KO");
  });

  app.post("/insert-smart-contract-data", async (req, res) => {
    const isSucceed = await createSmartContractAction();
    if (isSucceed) {
      return res.status(200).json("OK! smart contract data inserted!");
    }
    return res.status(500).json("KO");
  });
  return app.listen(process.env.PORT);
});

app.get("/events", async (req, res) => {
  const filters = req.query;
  const filterEventId = parseInt(filters.eventId as string);

  if (filters.eventId) {
    const filterEventId = parseInt(filters.eventId as string);
    if (!isNaN(filterEventId)) {
      const events = await getEvents({ eventId: filterEventId });
      return res.status(200).json(events);
    }
    return res.status(500).json("KO! Invalid Event id");
  }

  if (filters.saleStartAfter) {
    const saleStartAfter = filters.saleStartAfter as string;
    console.log(saleStartAfter);
    const filterSaleStartDate = moment(saleStartAfter).isValid();
    if (filterSaleStartDate) {
      const filterSaleStartDateInDate = new Date(
        moment(saleStartAfter).format(globalDateFormat)
      );
      const events = await getEvents({
        saleStartAfter: filterSaleStartDateInDate,
      });
      return res.status(200).json(events);
    }
    return res.status(500).json("KO! Invalid Date");
  }

  const events = await getEvents();
  if (events) {
    return res.status(200).json(events);
  }
  return res.status(500).json("KO");
});

app.put("/events/:id", async (req, res) => {
  const eventIdToUpdate = req.params.id;
  const content = req.body;
  if (!content || !eventIdToUpdate) {
    return res.status(400).json("ID or Body content can NOT be empty");
  }

  const eventIdToUpdateInNumber = parseInt(eventIdToUpdate);
  if (isNaN(eventIdToUpdateInNumber)) {
    return res.status(400).json("ID must be number");
  }

  const fieldsCanBeUpdated = (({
    eventTitle,
    lineUp,
    media,
    collectionName,
  }) => ({
    eventTitle,
    lineUp,
    media,
    collectionName,
  }))(content);

  const events = await updateEvent({
    eventId: eventIdToUpdateInNumber,
    body: {
      organizer: {
        eventTitle: fieldsCanBeUpdated.eventTitle,
        lineUp: fieldsCanBeUpdated.lineUp,
        media: fieldsCanBeUpdated.media,
      },
      collectionName: fieldsCanBeUpdated.collectionName,
    },
  });

  if (isError(events)) {
    return res.status(500).json(events.message);
  }

  return res.status(200).json(events);
});

app.put("/events/:id", async (req, res) => {
  const eventIdToUpdate = req.params.id;
  const content = req.body;
  if (!content || !eventIdToUpdate) {
    return res.status(400).json("ID or Body content can NOT be empty");
  }

  const eventIdToUpdateInNumber = parseInt(eventIdToUpdate);
  if (isNaN(eventIdToUpdateInNumber)) {
    return res.status(400).json("ID must be number");
  }

  const fieldsCanBeUpdated = (({
    eventTitle,
    lineUp,
    media,
    collectionName,
  }) => ({
    eventTitle,
    lineUp,
    media,
    collectionName,
  }))(content);

  const events = await updateEvent({
    eventId: eventIdToUpdateInNumber,
    body: {
      organizer: {
        eventTitle: fieldsCanBeUpdated.eventTitle,
        lineUp: fieldsCanBeUpdated.lineUp,
        media: fieldsCanBeUpdated.media,
      },
      collectionName: fieldsCanBeUpdated.collectionName,
    },
  });

  if (isError(events)) {
    return res.status(500).json(events.message);
  }

  return res.status(200).json(events);
});
