import { Organizer } from "../organizer/organizer.entity";
import { SmartContract } from "../smart-contract/smart-contract.entity";
import { EventRow, TicketCollection } from "./event";

const convertOrganizerSmartContractToTicketCollections = (
  organizerSmartContracts: SmartContract[]
): TicketCollection[] => {
  return organizerSmartContracts.map((smartContract) => {
    return {
      collectionName: smartContract.collectionName,
      scAddress: smartContract.crowdsale,
      collectionAddress: smartContract.collection,
      pricePerToken: smartContract.pricePerToken,
      maxMintPerUser: smartContract.maxMintPerUser,
      saleSize: smartContract.saleSize,
    };
  });
};

export const convertSelectedOrganizerToEventRow = (
  organizers: Organizer[]
): EventRow[] => {
  return organizers.map((organizer) => {
    return {
      eventId: organizer.eventId,
      title: organizer.eventTitle,
      startDatetime: organizer.eventStartDate,
      endDatetime: organizer.eventEndDate,
      address: organizer.address,
      locationName: organizer.addressName,
      totalTicketsCount: organizer.totalTicketNumber,
      assetUrl: organizer.media,
      lineUp: organizer.lineUp,
      ticketCollections: organizer.smartContracts
        ? convertOrganizerSmartContractToTicketCollections(
            organizer.smartContracts
          )
        : [],
    };
  });
};
