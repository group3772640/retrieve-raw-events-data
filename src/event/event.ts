import { AppDataSource } from "../data-source";
import { Organizer } from "../organizer/organizer.entity";
import { SmartContract } from "../smart-contract/smart-contract.entity";
import { convertSelectedOrganizerToEventRow } from "./mapper";

export type TicketCollection = {
  collectionName: SmartContract["collectionName"];
  scAddress: SmartContract["crowdsale"];
  collectionAddress: SmartContract["collection"];
  pricePerToken: SmartContract["pricePerToken"];
  maxMintPerUser: SmartContract["maxMintPerUser"];
};

export type EventRow = {
  eventId: Organizer["eventId"];
  title: Organizer["eventTitle"];
  startDatetime: Organizer["eventStartDate"];
  endDatetime: Organizer["eventEndDate"];
  address: Organizer["address"];
  locationName: Organizer["addressName"];
  totalTicketsCount: Organizer["totalTicketNumber"];
  assetUrl: Organizer["media"];
  lineUp: Organizer["lineUp"];
  ticketCollections: TicketCollection[];
};

export async function getEvents(filter?: {
  eventId?: number;
  saleStartAfter?: Date;
}): Promise<EventRow[] | undefined> {
  const organizerRepository = AppDataSource.manager.getRepository(Organizer);
  try {
    let organizerWithContracts = await organizerRepository.find({
      relations: ["smartContracts"],
    });

    if (filter) {
      const { eventId, saleStartAfter } = filter;
      if (eventId) {
        organizerWithContracts = organizerWithContracts.filter(
          (organizerWithContract) => organizerWithContract.eventId === eventId
        );
      }
      if (saleStartAfter) {
        organizerWithContracts = organizerWithContracts.filter(
          (organizerWithContract) =>
            organizerWithContract.saleStartAt >= saleStartAfter
        );
      }
    }
    const events = convertSelectedOrganizerToEventRow(organizerWithContracts);
    return events;
  } catch (error) {
    console.log(error);
    return undefined;
  }
}

export async function updateEvent(payload: {
  eventId: number;
  body: {
    organizer: {
      eventTitle?: Organizer["eventTitle"];
      lineUp?: Organizer["lineUp"];
      media?: Organizer["media"];
    };
    collectionName?: string;
  };
}): Promise<EventRow[] | Error> {
  const organizerRepository = AppDataSource.manager.getRepository(Organizer);
  const smartContractRepository =
    AppDataSource.manager.getRepository(SmartContract);
  try {
    const organizer = await organizerRepository.findOne({
      where: { eventId: payload.eventId },
      relations: ["smartContracts"],
    });

    if (!organizer) {
      return Error("Event not found");
    }

    //update collectionName in smartContract
    if (
      payload.body.collectionName &&
      organizer.smartContracts &&
      organizer.smartContracts?.length !== 0
    ) {
      organizer.smartContracts.map(async (smartContract) => {
        await smartContractRepository.update(
          { id: smartContract.id },
          { collectionName: payload.body.collectionName }
        );
      });
    }

    //update eventTitle, lineUp, media in organizer
    const { eventTitle, lineUp, media } = payload.body.organizer;
    await organizerRepository.update(
      { eventId: organizer.eventId },
      {
        eventTitle: eventTitle ? eventTitle : organizer.eventTitle,
        lineUp: lineUp ? lineUp : organizer.lineUp,
        media: media ? media : organizer.media,
      }
    );

    const updatedOrganizerWithContract = await organizerRepository.findOne({
      where: { eventId: payload.eventId },
      relations: ["smartContracts"],
    });
    if (!updatedOrganizerWithContract) {
      return Error("Unknown Error");
    }

    const events = convertSelectedOrganizerToEventRow([
      updatedOrganizerWithContract,
    ]);
    return events;
  } catch (error) {
    console.log(error);
    return Error("Unknown Error");
  }
}

export function isError(value: any): value is Error {
  return typeof value === "object" && value !== null && "message" in value;
}
