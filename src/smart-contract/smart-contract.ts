import { AppDataSource } from "../data-source";
import { Organizer } from "../organizer/organizer.entity";
import { convertSmartContractImportRowToSmartContract } from "./mapper";
import { smartContractData } from "./read-smart-contract-date";
import { SmartContract } from "./smart-contract.entity";

export async function createSmartContractAction(): Promise<boolean> {
  const smartContractRepository =
    AppDataSource.manager.getRepository(SmartContract);
  const smartContractDataReadyToInsert =
    convertSmartContractImportRowToSmartContract(smartContractData);
  const organizerRepository = AppDataSource.manager.getRepository(Organizer);
  try {
    smartContractDataReadyToInsert.map(async (data) => {
      const linkedOrganizer = await organizerRepository.findOne({
        where: { eventId: data.eventId },
      });
      smartContractRepository.save({
        ...data,
        organizerId: linkedOrganizer?.id,
      });
    });
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}
