import { SmartContractRow } from "./read-smart-contract-date";
import { SmartContract } from "./smart-contract.entity";

export const convertSmartContractImportRowToSmartContract = (
  smartContractRows: SmartContractRow[]
): Partial<SmartContract>[] => {
  return smartContractRows.map((smartContractRow) => {
    return {
      eventId: smartContractRow.event_id,
      collectionName: smartContractRow.collection_name,
      crowdsale: smartContractRow.smart_contract.crowdsale,
      collection: smartContractRow.smart_contract.collection,
      multisig: smartContractRow.smart_contract.multisig,
      isPresale: smartContractRow.smart_contract.sale_params.is_presale,
      metadataList: smartContractRow.smart_contract.sale_params.metadata_list,
      pricePerToken:
        smartContractRow.smart_contract.sale_params.price_per_token,
      maxMintPerUser:
        smartContractRow.smart_contract.sale_params.max_mint_per_user,
      saleSize: smartContractRow.smart_contract.sale_params.sale_size,
      saleCurrency: smartContractRow.smart_contract.sale_params.sale_currency,
    };
  });
};
