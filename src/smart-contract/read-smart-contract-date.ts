import * as fs from "fs";
import * as path from "path";

const smartContractFilePath = path.resolve(
  __dirname,
  "../../smart-contracts-data.json"
);
const smartContractString = fs.readFileSync(smartContractFilePath, "utf-8");
export const smartContractData = JSON.parse(
  smartContractString
) as SmartContractRow[];

type SaleParam = {
  is_presale: boolean;
  metadata_list: string;
  price_per_token: number;
  max_mint_per_user: number;
  sale_size: number;
  sale_currency: {
    xtz: null;
  };
  start_time: number;
  end_time: number;
};

type SmartContractObject = {
  crowdsale: string;
  collection: string;
  multisig: string;
  sale_params: SaleParam;
};

export type SmartContractRow = {
  event_id: number;
  collection_name: string;
  smart_contract: SmartContractObject;
};
