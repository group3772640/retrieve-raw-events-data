import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Organizer } from "../organizer/organizer.entity";

type SaleCurrency = {
  xtz: string | null;
};

@Entity("smart-contract")
export class SmartContract {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(() => Organizer)
  @JoinColumn()
  organizer!: Organizer | undefined;
  static organizer = (): string => "organizer";

  @Column({ nullable: true })
  organizerId!: Organizer["id"] | null;

  @Column({ nullable: false })
  eventId: number;

  @Column({ nullable: false, type: "varchar" })
  collectionName: string;

  @Column({ nullable: false, type: "varchar" })
  crowdsale: string;

  @Column({ nullable: false, type: "varchar" })
  collection: string;

  @Column({ nullable: false, type: "varchar" })
  multisig: string;

  @Column({ nullable: false, type: "boolean" })
  isPresale: boolean;

  @Column({ nullable: false, type: "varchar" })
  metadataList: string;

  @Column({ nullable: false })
  pricePerToken: number;

  @Column({ nullable: false })
  maxMintPerUser: number;

  @Column({ nullable: false })
  saleSize: number;

  @Column({ nullable: false, type: "json" })
  saleCurrency: SaleCurrency;
}
