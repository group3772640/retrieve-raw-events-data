import moment from "moment";
import { Organizer } from "./organizer.entity";
import { OrganizerImportRow } from "./read-organizer-data";

const frDateFormat = "DD/MM/YYYY hh:mm:ss";
export const globalDateFormat = "YYYY-MM-DD hh:mm:ss";

export const convertOrganizerImportRowToOrganizer = (
  organizerImportRows: OrganizerImportRow[]
): Partial<Organizer>[] => {
  return organizerImportRows.map((organizerImportRow) => {
    const eventStartDate = new Date(
      moment(organizerImportRow.eventStartDate, frDateFormat).format(
        globalDateFormat
      )
    );
    const eventEndDate = new Date(
      moment(organizerImportRow.eventEndDate, frDateFormat).format(
        globalDateFormat
      )
    );
    const saleStartAt = new Date(
      moment(organizerImportRow.saleStartAt, frDateFormat).format(
        globalDateFormat
      )
    );

    return {
      eventId: organizerImportRow.eventId,
      eventTitle: organizerImportRow.eventTitle,
      eventStartDate,
      eventEndDate,
      addressName: organizerImportRow.addressName ?? undefined,
      address: organizerImportRow.address,
      totalTicketNumber: organizerImportRow.totalTicketNumber,
      maxTicketPerUser: organizerImportRow.maxTicketPerUser,
      saleStartAt,
      lineUp: organizerImportRow.lineUp
        ? organizerImportRow.lineUp.split(" ")
        : [],
      media: organizerImportRow.media,
    };
  });
};
