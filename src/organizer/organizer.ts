import { AppDataSource } from "../data-source";
import { convertOrganizerImportRowToOrganizer } from "./mapper";
import { Organizer } from "./organizer.entity";
import { organizerImportObject } from "./read-organizer-data";

export async function createOrganizerActionFromCSV(): Promise<boolean> {
  const postRepository = AppDataSource.manager.getRepository(Organizer);
  const organizerData = organizerImportObject();
  const organizerDataReadyToInsert =
    convertOrganizerImportRowToOrganizer(organizerData);

  try {
    organizerDataReadyToInsert.map(async (data) => {
      postRepository.save(data);
    });
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}
