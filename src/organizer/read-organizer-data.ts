import { parse } from "csv-parse";
import * as fs from "fs";
import * as path from "path";

const csvFilePath = path.resolve(__dirname, "../../organizers-data.csv");
const importedCSVData: any[] = [];
fs.createReadStream(csvFilePath)
  .pipe(parse({ delimiter: ",", from_line: 2 }))
  .on("data", function (row) {
    importedCSVData.push(row);
  })
  .on("end", function () {
    console.log("finished");
  })
  .on("error", function (error) {
    console.log(error.message);
  });

export type OrganizerImportRow = {
  eventId: number;
  eventTitle: string;
  eventStartDate: string;
  eventEndDate: string;
  addressName: string | null;
  address: string;
  totalTicketNumber: number;
  maxTicketPerUser: number;
  saleStartAt: string;
  lineUp: string | null;
  media: string;
};

export function organizerImportObject(): OrganizerImportRow[] {
  const keyArray = [
    "eventId",
    "eventTitle",
    "eventStartDate",
    "eventEndDate",
    "addressName",
    "address",
    "totalTicketNumber",
    "maxTicketPerUser",
    "saleStartAt",
    "lineUp",
    "media",
  ];
  const importedCSVDataObject = importedCSVData.map((data) => {
    const object: { [key: string]: number | string } = {};
    for (let i = 0; i < keyArray.length; i++) {
      object[keyArray[i]] = data[i];
    }
    return object as OrganizerImportRow;
  });
  return importedCSVDataObject;
}
