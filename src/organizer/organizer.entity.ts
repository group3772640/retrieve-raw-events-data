import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { SmartContract } from "../smart-contract/smart-contract.entity";

@Entity("organizer")
export class Organizer {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @OneToMany(() => SmartContract, (smartContract) => smartContract.organizer)
  smartContracts!: SmartContract[] | undefined;

  @Column({ nullable: false })
  eventId: number;

  @Column({ nullable: false, type: "varchar" })
  eventTitle: string;

  @Column({ nullable: false, type: "timestamp" })
  eventStartDate: Date;

  @Column({ nullable: false, type: "timestamp" })
  eventEndDate: Date;

  @Column({ nullable: true, type: "varchar" })
  addressName: string;

  @Column({ nullable: false, type: "varchar" })
  address: string;

  @Column({ nullable: false })
  totalTicketNumber: number;

  @Column({ nullable: false })
  maxTicketPerUser: number;

  @Column({ nullable: false, type: "timestamp" })
  saleStartAt: Date;

  @Column("text", { array: true })
  lineUp: string[];

  @Column({ nullable: true, type: "varchar" })
  media!: string | null;
}
